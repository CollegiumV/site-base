site-base
=========
Configures site specific features, including globally desired packages, site-specific configuration directories, and custom site scripts.

Supported Operating Systems
---------------------------
* CentOS 7
* Void Linux

Requirements
------------
No Requirements

Role Variables
--------------
None

Role Defaults
-------------
```
---                                                                                                                                                                                      site_root: <string> (the root directory for site specific configuration) /export/content/collegiumv
site_packages: a hash with a list of packages to install for each host
  void:
	- acl-progs
	- cyrus-sasl-modules-gssapi
	- dialog
	- htop
	- ncurses-term
	- openldap
	- tmux
	- zsh
  RedHat:
	- cyrus-sasl-gssapi
	- dialog
	- ncurses-term
	- openldap
	- tmux
	- zsh

```

Dependencies
------------
No Dependencies

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
